//
//  ViewController.swift
//  Testy
//
//  Created by Kyle Hickey on 2/05/15.
//  Copyright (c) 2015 Kyle Hickey. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
    @IBOutlet weak var clickMeButton: UIButton!

    
    @IBAction func clickMePressed(sender: UIButton) {
    }
    
    override func viewDidLoad() {
        
        let buttonImage = UIImage(named: "button-facebook-notext")
        self.clickMeButton.setImage(buttonImage, forState: UIControlState.Normal)
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

